Welcome to Hi-Lo World.

This is a non-traditional introductory course. You will learn C / C ++ programming video games. They are divided by levels:

- Level 1: you learn to use the screen and some basic aspects of C / C ++.

- Level 2: The first game is developed using modular programming.

As you advance in the level, new aspects of programming are incorporated: from the way of making comments to the way of creating efficient code.

To perform these exercises you need the following hardware:

- Arduino UNO
- TFT 240x240 ST7789 IPS SPI screen. https://www.amazon.es/TECNOIOT-ST7789-Display-Pulgadas-Pantalla/dp/B07QGPKWJ2/ref=sr_1_2?dchild=1&keywords=ST7789&qid=1623056225&sr=8-2
- 4 Buttons
- 4 Resistors 10k Ohms.

And the software you need:

- VSCode
- Platformio (VSCode extension)


Bienvenido a Hi-Lo World. 

Esto es un curso no tradicional de iniciación. Aprenderás  C/C++ programando videojuegos. Están divididos por niveles:

- Level 1: se aprende a usar la pantalla y algunos aspectos básicos de C/C++.

- Level 2: Se desarrolla el primer juego mediante programación modular.

Conforme se avanza en el nivel, se incorporan nuevos aspectos de la programación: desde la forma de hacer comentarios hasta la manera de crear código eficiente.

Para realizar estos ejercicios necesitas el siguiente hardware:

- Arduino UNO
- Pantalla TFT 240x240 ST7789 IPS SPI. https://www.amazon.es/TECNOIOT-ST7789-Display-Pulgadas-Pantalla/dp/B07QGPKWJ2/ref=sr_1_2?dchild=1&keywords=ST7789&qid=1623056225&sr=8-2
- 4 Botones
- 4 Resistencias 10k Ohms.

Y el programa que necesitas es:

- VSCode
- Platformio (extension de VSCode)

