#include <Arduino.h>
#include <Adafruit_GFX.h>  // Core graphics library by Adafruit
#include "Arduino_ST7789.h"  // Hardware-specific library for ST7789 (with or without CS pin)
#include <SPI.h>
#include <Adafruit_I2CDevice.h>

#define TFT_DC 8
#define TFT_RST 9
//#define TFT_CS    10 // only for displays with CS pin
#define TFT_MOSI 11  // for hardware SPI data pin (all of available pins)
#define TFT_SCLK 13  // for hardware SPI sclk pin (all of available pins)

/*************************************
 BUTTONS DEFINITION
 ************************************/

#define rightButton 2
#define leftButton 3
#define upButton 4
#define downButton 5

Arduino_ST7789 tft =
    Arduino_ST7789(TFT_DC, TFT_RST);  // for display without CS pin

/*************************************
 USER VARIABLES
 ************************************/
/* New variables need to be created for obstacles.*/

int16_t x_player_1 = 0;
int16_t y_player_1 = 0;

int16_t x_obstacle_1 = 0;
int16_t y_obstacle_1 = 25;
uint8_t obstacle_1_speed = 0;

int16_t x_obstacle_2 = 0;
int16_t y_obstacle_2 = 75;
uint16_t obstacle_2_speed = 1;

int16_t x_obstacle_3 = 0;
int16_t y_obstacle_3 = 125;
uint8_t obstacle_3_speed = 2;

int16_t x_obstacle_4 = 0;
int16_t y_obstacle_4 = 175;
uint8_t obstacle_4_speed = 1;

uint8_t i = 0;

/*************************************
 USER PROCEDURES
 ************************************/
/*Parameters can be introduced to a procedure within the parentheses. A
 * parameter is a variable that is used at some point in the procedure code.
 * This way of programming allows code to be reused with some customization.*/

void player_1_CheckRightButtonAndMove();
void player_1_CheckLeftButtonAndMove();
void player_1_CheckUpButtonAndMove();
void player_1_CheckDownButtonAndMove();

void obstacle_move(int16_t x_value, int16_t y_value);

void setup() {
  tft.init(240, 240);
  tft.fillScreen(BLACK);
  tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);

  pinMode(rightButton, INPUT);
  pinMode(leftButton, INPUT);
  pinMode(upButton, INPUT);
  pinMode(downButton, INPUT);
}

void loop() {
  player_1_CheckRightButtonAndMove();
  player_1_CheckLeftButtonAndMove();
  player_1_CheckUpButtonAndMove();
  player_1_CheckDownButtonAndMove();

  /*By changing the parameters that are introduced in the procedure, the code
   * can be reused for various obstacles. In addition you can perform simple
   * operations.
   */

  obstacle_move(x_obstacle_1 + i, y_obstacle_1);
  obstacle_move(x_obstacle_2 + i, y_obstacle_2);
  obstacle_move(x_obstacle_3 + i, y_obstacle_3);
  obstacle_move(x_obstacle_4 + i, y_obstacle_4);

  i++;

  if (i == 240) {
    i = 0;
  }

  delay(20);

  /* CHALLENGE:
  - Change speed of obstacles.
*/
}

// USER BUTTONS AND MOVE //

void player_1_CheckRightButtonAndMove() {
  /* It is possible to have an "if" inside another "if". Thus various conditions
  can be applied.  */

  // MOVING RIGHT //

  if (digitalRead(rightButton) == HIGH) {
    if (x_player_1 != 230) {
      x_player_1++;
      tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);
      tft.fillRect(x_player_1 - 10, y_player_1, 10, 10, BLACK);
    }
  }
}

void player_1_CheckLeftButtonAndMove() {
  // MOVING LEFT //

  if (digitalRead(leftButton) == HIGH) {
    if (x_player_1 != 0) {
      x_player_1--;
      tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);
      tft.fillRect(x_player_1 + 10, y_player_1, 10, 10, BLACK);
    }
  }
}

void player_1_CheckUpButtonAndMove() {
  /* Remember that the origin of coordinates (0,0) is located in the upper left
  corner. The lower right point is (240,240). To lower the rectangle, add 1 to
  the variable y.*/

  // MOVING UP //

  if (digitalRead(upButton) == HIGH) {
    if (y_player_1 != 0) {
      y_player_1--;
      tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);
      tft.fillRect(x_player_1, y_player_1 + 10, 10, 10, BLACK);
    }
  }
}

void player_1_CheckDownButtonAndMove() {
  // MOVING DOWN //

  if (digitalRead(downButton) == HIGH) {
    if (y_player_1 != 230) {
      y_player_1++;
      tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);
      tft.fillRect(x_player_1, y_player_1 - 10, 10, 10, BLACK);
    }
  }
}

// COMPUTER OBSTACLES MOVING //

void obstacle_move(int16_t x_value, int16_t y_value) {
  if (x_value <= 240) {
    tft.fillRect(x_value, y_value, 10, 10, BLUE);
    if (x_value >= 10) {
      tft.fillRect(x_value - 10, y_value, 10, 10, BLACK);
    }
  }
}
