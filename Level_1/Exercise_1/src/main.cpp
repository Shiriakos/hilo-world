#include <Arduino.h>
#include <Adafruit_GFX.h>  // Core graphics library by Adafruit
#include "Arduino_ST7789.h"  // Hardware-specific library for ST7789 (with or without CS pin)
#include <SPI.h>
#include <Adafruit_I2CDevice.h>

#define TFT_DC 8
#define TFT_RST 9
//#define TFT_CS    10 // only for displays with CS pin
#define TFT_MOSI 11  // for hardware SPI data pin (all of available pins)
#define TFT_SCLK 13  // for hardware SPI sclk pin (all of available pins)

Arduino_ST7789 tft =
    Arduino_ST7789(TFT_DC, TFT_RST);  // for display without CS pin

void setup() {
  tft.init(240, 240);
  tft.fillScreen(BLACK);
}

void loop() {
  /* Draw a red filled rectangle

  tft . fillRect ( x_coordinate , y_coordinate , width , high , color ) ;

  */

  tft.fillRect(0, 0, 10, 10, RED);

  /* Draw a blue horizontal line 10 pixels high

  tft . drawFastHLine  ( x_coordinate , y_coordinate , width , color ) ;

  */

  tft.drawFastHLine(0, 50, 240, BLUE);

  /* Draw a white vertical line 10 pixels high

  tft . drawFastVLine  ( x_coordinate , y_coordinate , width , color ) ;

  */

  tft.drawFastVLine(50, 0, 240, WHITE);

  /* Draw:

    - 20 Red filled rectangles.
    - 20 Blue horizontal lines.
    - 20 white vertical lines.

*/
}
