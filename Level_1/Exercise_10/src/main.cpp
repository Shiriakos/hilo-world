#include <Arduino.h>
#include <Adafruit_GFX.h>  // Core graphics library by Adafruit
#include "Arduino_ST7789.h"  // Hardware-specific library for ST7789 (with or without CS pin)
#include <SPI.h>
#include <Adafruit_I2CDevice.h>

#define TFT_DC 8
#define TFT_RST 9
//#define TFT_CS    10 // only for displays with CS pin
#define TFT_MOSI 11  // for hardware SPI data pin (all of available pins)
#define TFT_SCLK 13  // for hardware SPI sclk pin (all of available pins)

/*************************************
 BUTTONS DEFINITION
 ************************************/

#define rightButton 2
#define leftButton 3
#define upButton 4
#define downButton 5

Arduino_ST7789 tft =
    Arduino_ST7789(TFT_DC, TFT_RST);  // for display without CS pin

/*************************************
 USER VARIABLES
 ************************************/
/* New variables need to be created for obstacles.*/

int16_t x_player_1 = 0;
int16_t y_player_1 = 0;

int16_t x_obstacle_1 = 0;
int16_t y_obstacle_1 = 25;
uint8_t obstacle_1_speed = 1;

int16_t x_obstacle_2 = 0;
int16_t y_obstacle_2 = 75;
uint8_t obstacle_2_speed = 2;

int16_t x_obstacle_3 = 0;
int16_t y_obstacle_3 = 125;
uint8_t obstacle_3_speed = 1;

int16_t x_obstacle_4 = 0;
int16_t y_obstacle_4 = 175;
uint8_t obstacle_4_speed = 2;

/*************************************
 USER PROCEDURES
 ************************************/
/*A "procedure" is a small piece of code. They are used to be able to organize
the code more clearly. It also makes it easier to reuse the code. It is not
necessary to put "procedure" in the name. Here it is posted to help identify
that it is a procedure.*/

void player_1_CheckRightButtonAndMove();
void player_1_CheckLeftButtonAndMove();
void player_1_CheckUpButtonAndMove();
void player_1_CheckDownButtonAndMove();

void obstacle_1_move();
void obstacle_2_move();
void obstacle_3_move();
void obstacle_4_move();

void setup() {
  tft.init(240, 240);
  tft.fillScreen(BLACK);
  tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);

  pinMode(rightButton, INPUT);
  pinMode(leftButton, INPUT);
  pinMode(upButton, INPUT);
  pinMode(downButton, INPUT);
}

void loop() {
  /* For the procedure to be executed in the code, you have to put its name
  without "void" and with a ";"
  */

  player_1_CheckRightButtonAndMove();
  player_1_CheckLeftButtonAndMove();
  player_1_CheckUpButtonAndMove();
  player_1_CheckDownButtonAndMove();

  obstacle_1_move();
  obstacle_2_move();
  obstacle_3_move();
  obstacle_4_move();

  delay(20);
  /* Exercise:
  - Change speed and direction of obstacles.
*/
}

// USER BUTTONS AND MOVE //

void player_1_CheckRightButtonAndMove() {
  /* It is possible to have an "if" inside another "if". Thus various conditions
  can be applied.  */

  // MOVING RIGHT //

  if (digitalRead(rightButton) == HIGH) {
    if (x_player_1 != 230) {
      x_player_1++;
      tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);
      tft.fillRect(x_player_1 - 10, y_player_1, 10, 10, BLACK);
    }
  }
}

void player_1_CheckLeftButtonAndMove() {
  // MOVING LEFT //

  if (digitalRead(leftButton) == HIGH) {
    if (x_player_1 != 0) {
      x_player_1--;
      tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);
      tft.fillRect(x_player_1 + 10, y_player_1, 10, 10, BLACK);
    }
  }
}

void player_1_CheckUpButtonAndMove() {
  /* Remember that the origin of coordinates (0,0) is located in the upper left
  corner. The lower right point is (240,240). To lower the rectangle, add 1 to
  the variable y.*/

  // MOVING UP //

  if (digitalRead(upButton) == HIGH) {
    if (y_player_1 != 0) {
      y_player_1--;
      tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);
      tft.fillRect(x_player_1, y_player_1 + 10, 10, 10, BLACK);
    }
  }
}

void player_1_CheckDownButtonAndMove() {
  // MOVING DOWN //

  if (digitalRead(downButton) == HIGH) {
    if (y_player_1 != 230) {
      y_player_1++;
      tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);
      tft.fillRect(x_player_1, y_player_1 - 10, 10, 10, BLACK);
    }
  }
}

// COMPUTER OBSTACLES MOVING //

void obstacle_1_move() {
  tft.fillRect(x_obstacle_1, y_obstacle_1, 10, 10, BLUE);
  if (x_obstacle_1 >= 9) {
    tft.fillRect(x_obstacle_1 - 10, y_obstacle_1, 10, 10, BLACK);
  }
  x_obstacle_1 = x_obstacle_1 + obstacle_1_speed;
  if (x_obstacle_1 >= 241) {
    x_obstacle_1 = 0;
  }
}

void obstacle_2_move() {
  tft.fillRect(x_obstacle_2, y_obstacle_2, 10, 10, WHITE);
  if (x_obstacle_2 >= 9) {
    tft.fillRect(x_obstacle_2 - 10, y_obstacle_2, 10, 10, BLACK);
  }
  x_obstacle_2 = x_obstacle_2 + obstacle_2_speed;
  if (x_obstacle_2 >= 241) {
    x_obstacle_2 = 0;
  }
}

void obstacle_3_move() {
  tft.fillRect(x_obstacle_3, y_obstacle_3, 10, 10, WHITE);
  if (x_obstacle_3 >= 9) {
    tft.fillRect(x_obstacle_3 - 10, y_obstacle_3, 10, 10, BLACK);
  }
  x_obstacle_3 = x_obstacle_3 + obstacle_3_speed;
  if (x_obstacle_3 >= 241) {
    x_obstacle_3 = 0;
  }
}

void obstacle_4_move() {
  tft.fillRect(x_obstacle_4, y_obstacle_4, 10, 10, WHITE);
  if (x_obstacle_4 >= 9) {
    tft.fillRect(x_obstacle_4 - 10, y_obstacle_4, 10, 10, BLACK);
  }
  x_obstacle_4 = x_obstacle_4 + obstacle_4_speed;
  if (x_obstacle_4 >= 241) {
    x_obstacle_4 = 0;
  }
}
