#include <Arduino.h>
#include <Adafruit_GFX.h>  // Core graphics library by Adafruit
#include "Arduino_ST7789.h"  // Hardware-specific library for ST7789 (with or without CS pin)
#include <SPI.h>
#include <Adafruit_I2CDevice.h>

#define TFT_DC 8
#define TFT_RST 9
//#define TFT_CS    10 // only for displays with CS pin
#define TFT_MOSI 11  // for hardware SPI data pin (all of available pins)
#define TFT_SCLK 13  // for hardware SPI sclk pin (all of available pins)

/*************************************
 BUTTONS DEFINITION
 ************************************/

#define rightButton 2
#define leftButton 3
#define upButton 4
#define downButton 5

Arduino_ST7789 tft =
    Arduino_ST7789(TFT_DC, TFT_RST);  // for display without CS pin

/*************************************
 USER VARIABLES
 ************************************/
/* New variables need to be created for obstacles.*/

int16_t x_player_1 = 0;
int16_t y_player_1 = 0;

/*************************************
 USER STRUCTS
 ************************************/
/*It is a collection of one or more types of elements called fields. Each of
 * which can be a different data type.It is recommended to initialize the
 * variables to 0 to avoid possible errors in the code.*/

struct obstacle {
  uint16_t x_coordinate = 0;
  uint16_t y_coordinate = 0;
  uint8_t i_counter = 0;
  uint16_t initialTime = 0;
  uint16_t currentTime = 0;
  uint16_t waitTime = 0;  // Equals movement speed
};

// INITIATION OF OBSTACLES AS STRUCTS //

struct obstacle obstacle_1, obstacle_2, obstacle_3, obstacle_4;

/*************************************
 USER PROCEDURES
 ************************************/
/*Parameters can be introduced to a procedure within the parentheses. A
 * parameter is a variable that is used at some point in the procedure code.
 * This way of programming allows code to be reused with some customization.*/

void player_1_CheckRightButtonAndMove();
void player_1_CheckLeftButtonAndMove();
void player_1_CheckUpButtonAndMove();
void player_1_CheckDownButtonAndMove();

void obstacle_move(int16_t x_value, int16_t y_value);

void setup() {
  tft.init(240, 240);
  tft.fillScreen(BLACK);
  tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);

  pinMode(rightButton, INPUT);
  pinMode(leftButton, INPUT);
  pinMode(upButton, INPUT);
  pinMode(downButton, INPUT);

  obstacle_1.x_coordinate = 0;
  obstacle_1.y_coordinate = 50;

  obstacle_2.x_coordinate = 0;
  obstacle_2.y_coordinate = 100;

  obstacle_3.x_coordinate = 0;
  obstacle_3.y_coordinate = 150;

  obstacle_4.x_coordinate = 0;
  obstacle_4.y_coordinate = 200;

  obstacle_1.initialTime = millis();
  obstacle_2.initialTime = millis();
  obstacle_3.initialTime = millis();
  obstacle_4.initialTime = millis();

  obstacle_1.waitTime = 10;
  obstacle_2.waitTime = 7;
  obstacle_3.waitTime = 4;
  obstacle_4.waitTime = 1;
}

void loop() {
  obstacle_1.currentTime = millis();
  obstacle_2.currentTime = millis();
  obstacle_3.currentTime = millis();
  obstacle_4.currentTime = millis();

  player_1_CheckRightButtonAndMove();
  player_1_CheckLeftButtonAndMove();
  player_1_CheckUpButtonAndMove();
  player_1_CheckDownButtonAndMove();

  // OBSTACLE ONE //

  if (obstacle_1.currentTime - obstacle_1.initialTime >= obstacle_1.waitTime) {
    obstacle_move(obstacle_1.x_coordinate + obstacle_1.i_counter,
                  obstacle_1.y_coordinate);
    obstacle_1.i_counter++;
    obstacle_1.initialTime = millis();
  }

  if (obstacle_1.i_counter == 241) {
    obstacle_1.i_counter = 0;
  }

  // OBSTACLE TWO //

  if (obstacle_2.currentTime - obstacle_2.initialTime >= obstacle_2.waitTime) {
    obstacle_move(obstacle_2.x_coordinate + obstacle_2.i_counter,
                  obstacle_2.y_coordinate);
    obstacle_2.i_counter++;
    obstacle_2.initialTime = millis();
  }

  if (obstacle_2.i_counter == 241) {
    obstacle_2.i_counter = 0;
  }

  // OBSTACLE THREE //

  if (obstacle_3.currentTime - obstacle_3.initialTime >= obstacle_3.waitTime) {
    obstacle_move(obstacle_3.x_coordinate + obstacle_3.i_counter,
                  obstacle_3.y_coordinate);
    obstacle_3.i_counter++;
    obstacle_3.initialTime = millis();
  }

  if (obstacle_3.i_counter == 241) {
    obstacle_3.i_counter = 0;
  }

  // OBSTACLE FOUR //

  if (obstacle_4.currentTime - obstacle_4.initialTime >= obstacle_4.waitTime) {
    obstacle_move(obstacle_4.x_coordinate + obstacle_4.i_counter,
                  obstacle_4.y_coordinate);
    obstacle_4.i_counter++;
    obstacle_4.initialTime = millis();
  }

  if (obstacle_4.i_counter == 241) {
    obstacle_4.i_counter = 0;
  }

  /* Exercise:
  - Create a player struct and change the user bottons and move section.
*/
}

// USER BUTTONS AND MOVE //

void player_1_CheckRightButtonAndMove() {
  /* It is possible to have an "if" inside another "if". Thus various conditions
  can be applied.  */

  // MOVING RIGHT //

  if (digitalRead(rightButton) == HIGH) {
    if (x_player_1 != 230) {
      x_player_1++;
      tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);
      tft.fillRect(x_player_1 - 10, y_player_1, 10, 10, BLACK);
    }
  }
}

void player_1_CheckLeftButtonAndMove() {
  // MOVING LEFT //

  if (digitalRead(leftButton) == HIGH) {
    if (x_player_1 != 0) {
      x_player_1--;
      tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);
      tft.fillRect(x_player_1 + 10, y_player_1, 10, 10, BLACK);
    }
  }
}

void player_1_CheckUpButtonAndMove() {
  /* Remember that the origin of coordinates (0,0) is located in the upper left
  corner. The lower right point is (240,240). To lower the rectangle, add 1 to
  the variable y.*/

  // MOVING UP //

  if (digitalRead(upButton) == HIGH) {
    if (y_player_1 != 0) {
      y_player_1--;
      tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);
      tft.fillRect(x_player_1, y_player_1 + 10, 10, 10, BLACK);
    }
  }
}

void player_1_CheckDownButtonAndMove() {
  // MOVING DOWN //

  if (digitalRead(downButton) == HIGH) {
    if (y_player_1 != 230) {
      y_player_1++;
      tft.fillRect(x_player_1, y_player_1, 10, 10, WHITE);
      tft.fillRect(x_player_1, y_player_1 - 10, 10, 10, BLACK);
    }
  }
}

// COMPUTER OBSTACLES MOVING //

void obstacle_move(int16_t x_value, int16_t y_value) {
    tft.fillRect(x_value, y_value, 10, 10, BLUE);
      tft.fillRect(x_value - 10, y_value, 10, 10, BLACK);
}
