#include <Arduino.h>
#include <Adafruit_GFX.h>  // Core graphics library by Adafruit
#include "Arduino_ST7789.h"  // Hardware-specific library for ST7789 (with or without CS pin)
#include <SPI.h>
#include <Adafruit_I2CDevice.h>

#define TFT_DC 8
#define TFT_RST 9
//#define TFT_CS    10 // only for displays with CS pin
#define TFT_MOSI 11  // for hardware SPI data pin (all of available pins)
#define TFT_SCLK 13  // for hardware SPI sclk pin (all of available pins)

Arduino_ST7789 tft =
    Arduino_ST7789(TFT_DC, TFT_RST);  // for display without CS pin

void setup() {
  tft.init(240, 240);
  tft.fillScreen(BLACK);
}

void loop() {
  /* Move horizontally the red filled rectangle 15 pixels.

  tft . fillRect ( x_coordinate , y_coordinate , width , high , color ) ;

  */

  tft.fillRect(0, 0, 10, 10, RED);
  tft.fillRect(1, 0, 10, 10, RED);
  tft.fillRect(2, 0, 10, 10, RED);
  tft.fillRect(3, 0, 10, 10, RED);
  tft.fillRect(4, 0, 10, 10, RED);
  tft.fillRect(5, 0, 10, 10, RED);
  tft.fillRect(6, 0, 10, 10, RED);
  tft.fillRect(7, 0, 10, 10, RED);
  tft.fillRect(8, 0, 10, 10, RED);
  tft.fillRect(9, 0, 10, 10, RED);
  tft.fillRect(10, 0, 10, 10, RED);
  tft.fillRect(11, 0, 10, 10, RED);
  tft.fillRect(12, 0, 10, 10, RED);
  tft.fillRect(13, 0, 10, 10, RED);
  tft.fillRect(14, 0, 10, 10, RED);
  tft.fillRect(15, 0, 10, 10, RED);

  /* Move horizontally the WHITE filled rectangle 230 points.

  Use the "for" control loop to change the coordinates of the rectangle.

  "y_coordinate" is equal 50 to draw the rectangle at position 50.

  tft . fillRect ( x_coordinate , y_coordinate , width , high , color ) ;

  */

  for (uint8_t x = 0; x <= 230; x++) {
    tft.fillRect(x, 50, 10, 10, WHITE);
  }

  /* Move horizontally the BLUE filled rectangle 240 points using "delay" to
  change the speed of the drawing.

  "delay ( time )" pauses code execution for the specified time in ms.

  Example:

  "delay ( 1000 )" = 1 second pause.

  "delay ( 100 )"  = 0.1 second pause = 100 ms pause.

  */

  for (uint8_t x = 0; x < 230; x++) {
    tft.fillRect(x, 100, 10, 10, BLUE);

    delay(100);
  }

  /* Draw:

  - Move vertically the BLUE filled rectangle 240 points.

  */
}
