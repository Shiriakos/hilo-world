#include <Arduino.h>
#include <Adafruit_GFX.h>  // Core graphics library by Adafruit
#include "Arduino_ST7789.h"  // Hardware-specific library for ST7789 (with or without CS pin)
#include <SPI.h>
#include <Adafruit_I2CDevice.h>

#define TFT_DC 8
#define TFT_RST 9
//#define TFT_CS    10 // only for displays with CS pin
#define TFT_MOSI 11  // for hardware SPI data pin (all of available pins)
#define TFT_SCLK 13  // for hardware SPI sclk pin (all of available pins)

Arduino_ST7789 tft =
    Arduino_ST7789(TFT_DC, TFT_RST);  // for display without CS pin

void setup() {
  tft.init(240, 240);
  tft.fillScreen(BLACK);
}

void loop() {
  /* Move horizontally the WHITE filled rectangle 240 points but starting at
  x = 230, ending at x = 0 and y = 0.

  Use the "for" control loop to change the coordinates of the rectangle.

  */

  for (int16_t x = 230; x >= 0; x--) {
    tft.fillRect(x, 0, 10, 10, WHITE);

    delay(10);
  }

  /* Move horizontally the WHITE filled rectangle 240 points but starting at
  x = 230, ending at x = 0 and y = 50.

  Use the "for" control loop to change the coordinates of the rectangle.

  */

  for (int16_t x = 230; x >= 0; x--) {
    tft.fillRect(x, 50, 10, 10, WHITE);

    delay(10);
  }

  /* Move horizontally the WHITE filled rectangle 240 points but starting at
  x = 230, ending at x = 0 and y = 100.

  Use the "for" control loop to change the coordinates of the rectangle.

  */

  for (int16_t x = 230; x >= 0; x--) {
    tft.fillRect(x, 100, 10, 10, WHITE);

    delay(10);
  }

  /* Move horizontally the WHITE filled rectangle 240 points but starting at
  x = 230, ending at x = 0 and y = 150.

  Use the "for" control loop to change the coordinates of the rectangle.

  */

  for (int16_t x = 230; x >= 0; x--) {
    tft.fillRect(x, 150, 10, 10, WHITE);

    delay(10);
  }

  /* Draw:

  - Draw vertically the WHITE filled starting with 230 and ending with 0 as you
  have done in the previous examples.

  */
}
