#include <Arduino.h>
#include <Adafruit_GFX.h>  // Core graphics library by Adafruit
#include "Arduino_ST7789.h"  // Hardware-specific library for ST7789 (with or without CS pin)
#include <SPI.h>
#include <Adafruit_I2CDevice.h>

#define TFT_DC 8
#define TFT_RST 9
//#define TFT_CS    10 // only for displays with CS pin
#define TFT_MOSI 11  // for hardware SPI data pin (all of available pins)
#define TFT_SCLK 13  // for hardware SPI sclk pin (all of available pins)

/*************************************
 BUTTONS DEFINITION
 ************************************/

#define leftButton 0
#define rightButton 1
#define upButton 2
#define downButton 3

Arduino_ST7789 tft =
    Arduino_ST7789(TFT_DC, TFT_RST);  // for display without CS pin

/*************************************
 USER VARIABLES
 ************************************/

uint16_t x = 0;
uint16_t y = 0;

void setup() {
  tft.init(240, 240);
  tft.fillScreen(BLACK);
  pinMode(2, INPUT);
}

void loop() {
  /* "If" is a control structure that a code snippet will execute if it meets a
  condition.

  In this code "x" is added 1 if the state of the pin is HIGH. This is achieved
  when the right button is pressed.
  */

  // MOVING RIGHT //

  if (digitalRead(rightButton) == HIGH) {
    x++;
    tft.fillRect(x, 0, 10, 10, WHITE);
    tft.fillRect(x - 10, 0, 10, 10, BLACK);
  }

  // MOVING LEFT //

  if (digitalRead(leftButton) == HIGH) {
    x--;
    tft.fillRect(x, 0, 10, 10, WHITE);
    tft.fillRect(x + 10, 0, 10, 10, BLACK);
  }

  /* Remember that the origin of coordinates (0,0) is located in the upper left
  corner. The lower right point is (240,240). To lower the rectangle, add 1 to
  the variable y.*/

  // MOVING UP //

  if (digitalRead(upButton) == HIGH) {
    y--;
    tft.fillRect(x, y, 10, 10, WHITE);
    tft.fillRect(x - 10, y + 10, 10, 10, BLACK);
  }

  // MOVING DOWN //

  if (digitalRead(downButton) == HIGH) {
    y++;
    tft.fillRect(x, y, 10, 10, WHITE);
    tft.fillRect(x - 10, y - 10, 10, 10, BLACK);
  }

  /* Exercise:

  - Change the velocity of displacement

*/
}
