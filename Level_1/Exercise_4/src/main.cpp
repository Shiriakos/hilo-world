#include <Arduino.h>
#include <Adafruit_GFX.h>  // Core graphics library by Adafruit
#include "Arduino_ST7789.h"  // Hardware-specific library for ST7789 (with or without CS pin)
#include <SPI.h>
#include <Adafruit_I2CDevice.h>

#define TFT_DC 8
#define TFT_RST 9
//#define TFT_CS    10 // only for displays with CS pin
#define TFT_MOSI 11  // for hardware SPI data pin (all of available pins)
#define TFT_SCLK 13  // for hardware SPI sclk pin (all of available pins)

Arduino_ST7789 tft =
    Arduino_ST7789(TFT_DC, TFT_RST);  // for display without CS pin

void setup() {
  tft.init(240, 240);
  tft.fillScreen(BLACK);
}

void loop() {
  /* Use the for loop to resize the WHITE filled rectangle at x = 0 and y = 0.
    "uint8_t i" is a variable counter.
  */

  for (int16_t i = 0; i < 240; i = i + 50) {
    tft.fillRect(0, 0, i, i, WHITE);
  }

  /* Do the same but with a 10 x 10 BLUE rectangle. The Y coordinate needs to go
  in steps of 10 points.

  */

  for (int16_t i = 0; i <= 240; i = i + 10) {
    tft.fillRect(0, 0, i, i, BLUE);
  }

  /* Exercise:

  - Do the same but starting at 239 and ending at 0.

*/
}
