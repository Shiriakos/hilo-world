#include <Arduino.h>
#include <Adafruit_GFX.h>  // Core graphics library by Adafruit
#include "Arduino_ST7789.h"  // Hardware-specific library for ST7789 (with or without CS pin)
#include <SPI.h>
#include <Adafruit_I2CDevice.h>

#define TFT_DC 8
#define TFT_RST 9
//#define TFT_CS    10 // only for displays with CS pin
#define TFT_MOSI 11  // for hardware SPI data pin (all of available pins)
#define TFT_SCLK 13  // for hardware SPI sclk pin (all of available pins)

Arduino_ST7789 tft =
    Arduino_ST7789(TFT_DC, TFT_RST);  // for display without CS pin

void setup() {
  tft.init(240, 240);
  tft.fillScreen(BLACK);
}

void loop() {
  /* Use the for loop to draw a 10 pixel wide WHITE frame.
  We don´t need to end at 240 because the filled rectangle has 10 pixel wide.
  */

  for (uint8_t x = 0; x < 240; x++) {
    tft.fillRect(x, 0, 10, 10, WHITE);
  }

  for (uint8_t y = 0; y < 230; y++) {
    tft.fillRect(230, y, 10, 10, WHITE);
  }

  for (uint8_t x = 230; x > 0; x--) {
    tft.fillRect(x, 230, 10, 10, WHITE);
  }

  for (uint8_t y = 230; y >= 0; y--) {
    tft.fillRect(0, y, 10, 10, WHITE);
  }

  /* Exercise:

  - Draw new frames inside.

*/
}
