#include <CPU_NPC.h>

struct NPC NPC_1, NPC_2, NPC_3, NPC_4, NPC_5;

void NPC_set(uint8_t NPC_select, int16_t x_coordinate_init,
             int16_t y_coordinate_init, int16_t wide, int16_t high,
             uint16_t colour, uint16_t speed) {
  if (NPC_select == 1) {
    NPC_1.x_coordinate_init = x_coordinate_init;
    NPC_1.y_coordinate_init = y_coordinate_init;
    NPC_1.x_coordinate = 0;
    NPC_1.y_coordinate = 0;
    NPC_1.wide = wide;
    NPC_1.high = high;
    NPC_1.colour = colour;
    NPC_1.speed = speed;
  }
  if (NPC_select == 2) {
    NPC_2.x_coordinate_init = x_coordinate_init;
    NPC_2.y_coordinate_init = y_coordinate_init;
    NPC_2.x_coordinate = 0;
    NPC_2.y_coordinate = 0;
    NPC_2.wide = wide;
    NPC_2.high = high;
    NPC_2.colour = colour;
    NPC_2.speed = speed;
  }
  if (NPC_select == 3) {
    NPC_3.x_coordinate_init = x_coordinate_init;
    NPC_3.y_coordinate_init = y_coordinate_init;
    NPC_3.x_coordinate = 0;
    NPC_3.y_coordinate = 0;
    NPC_3.wide = wide;
    NPC_3.high = high;
    NPC_3.colour = colour;
    NPC_3.speed = speed;
  }

  if (NPC_select == 4) {
    NPC_4.x_coordinate_init = x_coordinate_init;
    NPC_4.y_coordinate_init = y_coordinate_init;
    NPC_4.x_coordinate = 0;
    NPC_4.y_coordinate = 0;
    NPC_4.wide = wide;
    NPC_4.high = high;
    NPC_4.colour = colour;
    NPC_4.speed = speed;
  }
  if (NPC_select == 5) {
    NPC_5.x_coordinate_init = x_coordinate_init;
    NPC_5.y_coordinate_init = y_coordinate_init;
    NPC_5.x_coordinate = 0;
    NPC_5.y_coordinate = 0;
    NPC_5.wide = wide;
    NPC_5.high = high;
    NPC_5.colour = colour;
    NPC_5.speed = speed;
  }
  // If your game has more players you will need more "if(NPC_select == ...)"
}

void NPC_init(int16_t x_coordinate_init, int16_t y_coordinate_init,
              int16_t wide, int16_t high, uint16_t colour) {
  tft.fillRect(x_coordinate_init, y_coordinate_init, wide, high, colour);
}

void NPC_move(uint8_t NPC_select, int16_t x_coordinate, int16_t y_coordinate,
              char direction) {
  if (NPC_select == 1) {
    if (direction == 'l') {
      NPC_1.x_coordinate = NPC_1.x_coordinate_init - x_coordinate;
      NPC_1.y_coordinate = NPC_1.y_coordinate_init + y_coordinate;
      if (x_coordinate <= 240) {
        tft.fillRect(NPC_1.x_coordinate, NPC_1.y_coordinate, NPC_1.wide,
                     NPC_1.high, NPC_1.colour);
      }
      tft.fillRect(NPC_1.x_coordinate + 10, NPC_1.y_coordinate, NPC_1.wide,
                   NPC_1.high, BLACK);
    }
    if (direction == 'r') {
      NPC_1.x_coordinate = NPC_1.x_coordinate_init + x_coordinate;
      NPC_1.y_coordinate = NPC_1.y_coordinate_init + y_coordinate;
      tft.fillRect(NPC_1.x_coordinate, NPC_1.y_coordinate, NPC_1.wide,
                   NPC_1.high, NPC_1.colour);
      tft.fillRect(NPC_1.x_coordinate - 10, NPC_1.y_coordinate, NPC_1.wide,
                   NPC_1.high, BLACK);
    }
    if (direction == 'u') {
      NPC_1.x_coordinate = NPC_1.x_coordinate_init + x_coordinate;
      NPC_1.y_coordinate = NPC_1.y_coordinate_init - y_coordinate;
      if (y_coordinate <= 240) {
        tft.fillRect(NPC_1.x_coordinate, NPC_1.y_coordinate, NPC_1.wide,
                     NPC_1.high, NPC_1.colour);
      }
      tft.fillRect(NPC_1.y_coordinate + 10, NPC_1.y_coordinate, NPC_1.wide,
                   NPC_1.high, BLACK);
    }

    if (direction == 'd') {
      NPC_1.x_coordinate = NPC_1.x_coordinate_init + x_coordinate;
      NPC_1.y_coordinate = NPC_1.y_coordinate_init + y_coordinate;
      tft.fillRect(NPC_1.x_coordinate, NPC_1.y_coordinate, NPC_1.wide,
                   NPC_1.high, NPC_1.colour);
      tft.fillRect(NPC_1.x_coordinate, NPC_1.y_coordinate - 10, NPC_1.wide,
                   NPC_1.high, BLACK);
    }
  }
  if (NPC_select == 2) {
    if (direction == 'l') {
      NPC_2.x_coordinate = NPC_2.x_coordinate_init - x_coordinate;
      NPC_2.y_coordinate = NPC_2.y_coordinate_init + y_coordinate;
      if (x_coordinate <= 240) {
        tft.fillRect(NPC_2.x_coordinate, NPC_2.y_coordinate, NPC_2.wide,
                     NPC_2.high, NPC_2.colour);
      }
      tft.fillRect(NPC_2.x_coordinate + 10, NPC_2.y_coordinate, NPC_2.wide,
                   NPC_2.high, BLACK);
    }
    if (direction == 'r') {
      NPC_2.x_coordinate = NPC_2.x_coordinate_init + x_coordinate;
      NPC_2.y_coordinate = NPC_2.y_coordinate_init + y_coordinate;
      tft.fillRect(NPC_2.x_coordinate, NPC_2.y_coordinate, NPC_2.wide,
                   NPC_2.high, NPC_2.colour);
      tft.fillRect(NPC_2.x_coordinate - 10, NPC_2.y_coordinate, NPC_2.wide,
                   NPC_2.high, BLACK);
    }
    if (direction == 'u') {
      NPC_2.x_coordinate = NPC_2.x_coordinate_init + x_coordinate;
      NPC_2.y_coordinate = NPC_2.y_coordinate_init - y_coordinate;
      if (y_coordinate <= 240) {
        tft.fillRect(NPC_2.x_coordinate, NPC_2.y_coordinate, NPC_2.wide,
                     NPC_2.high, NPC_2.colour);
      }
      tft.fillRect(NPC_2.y_coordinate + 10, NPC_2.y_coordinate, NPC_2.wide,
                   NPC_2.high, BLACK);
    }

    if (direction == 'd') {
      NPC_2.x_coordinate = NPC_2.x_coordinate_init + x_coordinate;
      NPC_2.y_coordinate = NPC_2.y_coordinate_init + y_coordinate;
      tft.fillRect(NPC_2.x_coordinate, NPC_2.y_coordinate, NPC_2.wide,
                   NPC_2.high, NPC_2.colour);
      tft.fillRect(NPC_2.x_coordinate, NPC_2.y_coordinate - 10, NPC_2.wide,
                   NPC_2.high, BLACK);
    }
  }
    if (NPC_select == 3) {
    if (direction == 'l') {
      NPC_3.x_coordinate = NPC_3.x_coordinate_init - x_coordinate;
      NPC_3.y_coordinate = NPC_3.y_coordinate_init + y_coordinate;
      if (x_coordinate <= 240) {
        tft.fillRect(NPC_3.x_coordinate, NPC_3.y_coordinate, NPC_3.wide,
                     NPC_3.high, NPC_3.colour);
      }
      tft.fillRect(NPC_3.x_coordinate + 10, NPC_3.y_coordinate, NPC_3.wide,
                   NPC_3.high, BLACK);
    }
    if (direction == 'r') {
      NPC_3.x_coordinate = NPC_3.x_coordinate_init + x_coordinate;
      NPC_3.y_coordinate = NPC_3.y_coordinate_init + y_coordinate;
      tft.fillRect(NPC_3.x_coordinate, NPC_3.y_coordinate, NPC_3.wide,
                   NPC_3.high, NPC_3.colour);
      tft.fillRect(NPC_3.x_coordinate - 10, NPC_3.y_coordinate, NPC_3.wide,
                   NPC_3.high, BLACK);
    }
    if (direction == 'u') {
      NPC_3.x_coordinate = NPC_3.x_coordinate_init + x_coordinate;
      NPC_3.y_coordinate = NPC_3.y_coordinate_init - y_coordinate;
      if (y_coordinate <= 240) {
        tft.fillRect(NPC_3.x_coordinate, NPC_3.y_coordinate, NPC_3.wide,
                     NPC_3.high, NPC_3.colour);
      }
      tft.fillRect(NPC_3.y_coordinate + 10, NPC_3.y_coordinate, NPC_3.wide,
                   NPC_3.high, BLACK);
    }

    if (direction == 'd') {
      NPC_3.x_coordinate = NPC_3.x_coordinate_init + x_coordinate;
      NPC_3.y_coordinate = NPC_3.y_coordinate_init + y_coordinate;
      tft.fillRect(NPC_3.x_coordinate, NPC_3.y_coordinate, NPC_3.wide,
                   NPC_3.high, NPC_3.colour);
      tft.fillRect(NPC_3.x_coordinate, NPC_3.y_coordinate - 10, NPC_3.wide,
                   NPC_3.high, BLACK);
    }
  }
    if (NPC_select == 4) {
    if (direction == 'l') {
      NPC_4.x_coordinate = NPC_4.x_coordinate_init - x_coordinate;
      NPC_4.y_coordinate = NPC_4.y_coordinate_init + y_coordinate;
      if (x_coordinate <= 240) {
        tft.fillRect(NPC_4.x_coordinate, NPC_4.y_coordinate, NPC_4.wide,
                     NPC_4.high, NPC_4.colour);
      }
      tft.fillRect(NPC_4.x_coordinate + 10, NPC_4.y_coordinate, NPC_4.wide,
                   NPC_4.high, BLACK);
    }
    if (direction == 'r') {
      NPC_4.x_coordinate = NPC_4.x_coordinate_init + x_coordinate;
      NPC_4.y_coordinate = NPC_4.y_coordinate_init + y_coordinate;
      tft.fillRect(NPC_4.x_coordinate, NPC_4.y_coordinate, NPC_4.wide,
                   NPC_4.high, NPC_4.colour);
      tft.fillRect(NPC_4.x_coordinate - 10, NPC_4.y_coordinate, NPC_4.wide,
                   NPC_4.high, BLACK);
    }
    if (direction == 'u') {
      NPC_4.x_coordinate = NPC_4.x_coordinate_init + x_coordinate;
      NPC_4.y_coordinate = NPC_4.y_coordinate_init - y_coordinate;
      if (y_coordinate <= 240) {
        tft.fillRect(NPC_4.x_coordinate, NPC_4.y_coordinate, NPC_4.wide,
                     NPC_4.high, NPC_4.colour);
      }
      tft.fillRect(NPC_4.y_coordinate + 10, NPC_4.y_coordinate, NPC_4.wide,
                   NPC_4.high, BLACK);
    }

    if (direction == 'd') {
      NPC_4.x_coordinate = NPC_4.x_coordinate_init + x_coordinate;
      NPC_4.y_coordinate = NPC_4.y_coordinate_init + y_coordinate;
      tft.fillRect(NPC_4.x_coordinate, NPC_4.y_coordinate, NPC_4.wide,
                   NPC_4.high, NPC_4.colour);
      tft.fillRect(NPC_4.x_coordinate, NPC_4.y_coordinate - 10, NPC_4.wide,
                   NPC_4.high, BLACK);
    }
  }
    if (NPC_select == 5) {
    if (direction == 'l') {
      NPC_5.x_coordinate = NPC_5.x_coordinate_init - x_coordinate;
      NPC_5.y_coordinate = NPC_5.y_coordinate_init + y_coordinate;
      if (x_coordinate <= 240) {
        tft.fillRect(NPC_5.x_coordinate, NPC_5.y_coordinate, NPC_5.wide,
                     NPC_5.high, NPC_5.colour);
      }
      tft.fillRect(NPC_5.x_coordinate + 10, NPC_5.y_coordinate, NPC_5.wide,
                   NPC_5.high, BLACK);
    }
    if (direction == 'r') {
      NPC_5.x_coordinate = NPC_5.x_coordinate_init + x_coordinate;
      NPC_5.y_coordinate = NPC_5.y_coordinate_init + y_coordinate;
      tft.fillRect(NPC_5.x_coordinate, NPC_5.y_coordinate, NPC_5.wide,
                   NPC_5.high, NPC_5.colour);
      tft.fillRect(NPC_5.x_coordinate - 10, NPC_5.y_coordinate, NPC_5.wide,
                   NPC_5.high, BLACK);
    }
    if (direction == 'u') {
      NPC_5.x_coordinate = NPC_5.x_coordinate_init + x_coordinate;
      NPC_5.y_coordinate = NPC_5.y_coordinate_init - y_coordinate;
      if (y_coordinate <= 240) {
        tft.fillRect(NPC_5.x_coordinate, NPC_5.y_coordinate, NPC_5.wide,
                     NPC_5.high, NPC_5.colour);
      }
      tft.fillRect(NPC_5.y_coordinate + 10, NPC_5.y_coordinate, NPC_5.wide,
                   NPC_5.high, BLACK);
    }

    if (direction == 'd') {
      NPC_5.x_coordinate = NPC_5.x_coordinate_init + x_coordinate;
      NPC_5.y_coordinate = NPC_5.y_coordinate_init + y_coordinate;
      tft.fillRect(NPC_5.x_coordinate, NPC_5.y_coordinate, NPC_5.wide,
                   NPC_5.high, NPC_5.colour);
      tft.fillRect(NPC_5.x_coordinate, NPC_5.y_coordinate - 10, NPC_5.wide,
                   NPC_5.high, BLACK);
    }
  }
  // If your game has more NPCs you will need more "if(NPC_select == ...)"
}
