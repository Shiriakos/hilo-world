#ifndef _IMPACT_CHECK_
#define _IMPACT_CHECK_

#include <Arduino.h>
#include <Arduino_ST7789.h>
#include <Player.h>
#include <CPU_NPC.h>

#pragma once

int8_t impactCheck();

#endif
