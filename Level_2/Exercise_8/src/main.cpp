/**
 * LEVEL 2 - EXERCISE 8
 *
 * @author Shiris S.G.
 * @version 0.8.0 (game reference)
 * @date 2021/06/26
 * @brief Set up and down limits for player 1, created 4 NPCs and disabled
 * impact check (it will be activaded in next version).
 *
 *
 *
 *
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the tearms of the GNU General Public License as published by the Free
 * Software Fundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 */

#include <Arduino.h>
#include <Adafruit_GFX.h>  // Core graphics library by Adafruit
#include <Arduino_ST7789.h>  // Hardware-specific library for ST7789 (with or without CS pin)
#include <SPI.h>
#include <Adafruit_I2CDevice.h>
#include <Player.h>
#include <Screen_configuration.h>
#include <CPU_NPC.h>
#include <Impact_Check.h>
#include <Scene.h>

/*************************************
 USER MACROS
 ************************************/

#define Player_1_speed 20
#define NPC_1_speed 9
#define NPC_2_speed 9
#define NPC_3_speed 10
#define NPC_4_speed 10
#define NPC_5_speed 15

/*************************************
 USER VARIABLES
 ************************************/

int16_t i1 = 0, i2 = 0, i3 = 0, i4 = 0, i5 = 0;
uint16_t currentTime = 0;

char LEFT = 'l';
char RIGHT = 'r';
char UP = 'u';
char DOWN = 'd';

/****************************************/

void setup() {
  init_screen(BLACK);

  NPC_set(1, 240, 40, 10, 32, GREEN, NPC_1_speed);
  NPC_set(2, 240, 72, 10, 32, GREEN, NPC_2_speed);
  NPC_set(3, 240, 104, 10, 32, GREEN, NPC_3_speed);
  NPC_set(4, 240, 136, 10, 32, GREEN, NPC_4_speed);
  NPC_set(5, 240, 168, 10, 32, GREEN, NPC_5_speed);

  NPC_init(NPC_1.x_coordinate_init, NPC_1.y_coordinate_init, NPC_1.wide,
           NPC_1.high, NPC_1.colour);
  NPC_init(NPC_2.x_coordinate_init, NPC_2.y_coordinate_init, NPC_2.wide,
           NPC_2.high, NPC_2.colour);
  NPC_init(NPC_3.x_coordinate_init, NPC_3.y_coordinate_init, NPC_3.wide,
           NPC_3.high, NPC_3.colour);
  NPC_init(NPC_4.x_coordinate_init, NPC_4.y_coordinate_init, NPC_4.wide,
           NPC_4.high, NPC_4.colour);
  NPC_init(NPC_5.x_coordinate_init, NPC_5.y_coordinate_init, NPC_5.wide,
           NPC_5.high, NPC_5.colour);

  ground_6x6(GREEN);

  player_set(1, 0, 51, 10, 10, WHITE, Player_1_speed);

  player_init(player_1.x_coordinate_init, player_1.y_coordinate_init,
              player_1.wide, player_1.high, player_1.colour);
}

void loop() {
  currentTime = millis();

  if (currentTime - player_1.initTime >= player_1.speed) {
    player_move();
    player_1.initTime = millis();
  }

  if (currentTime - NPC_1.initTime >= NPC_1.speed) {
    NPC_move(1, i1, 0, LEFT);  //
    NPC_1.initTime = millis();
    i1++;
    if (i1 == 251) {
      i1 = 0;
    }
  }
  if (currentTime - NPC_2.initTime >= NPC_2.speed) {
    NPC_move(2, i2, 0, LEFT);  //
    NPC_2.initTime = millis();
    i2++;
    if (i2 == 251) {
      i2 = 0;
    }
  }
  if (currentTime - NPC_3.initTime >= NPC_3.speed) {
    NPC_move(3, i3, 0, LEFT);  //
    NPC_3.initTime = millis();
    i3++;
    if (i3 == 251) {
      i3 = 0;
    }
  }
  if (currentTime - NPC_4.initTime >= NPC_4.speed) {
    NPC_move(4, i4, 0, LEFT);  //
    NPC_4.initTime = millis();
    i4++;
    if (i4 == 251) {
      i4 = 0;
    }
  }
  if (currentTime - NPC_5.initTime >= NPC_5.speed) {
    NPC_move(5, i5, 0, LEFT);  //
    NPC_5.initTime = millis();
    i5++;
    if (i5 == 251) {
      i5 = 0;
    }
  }
  /*
    if (impactCheck() == 1) {
      i1 = 0;
    }
    */
}

/* Exercise:
- Draw new scenarios
*/
