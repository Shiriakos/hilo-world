/* Here are the procedures and functions for the player. */

#include <Arduino.h>
#include <Arduino_ST7789.h>

#ifndef _PLAYER_
#define _PLAYER_

#pragma once

/*************************************
 BUTTON DEFINITION
 ************************************/

#define rightButton 2
#define leftButton 3
#define upButton 4
#define downButton 5

/*************************************
 PLAYER DEFINITION
 ************************************/

struct player {
  int16_t x_coordinate_init = 0;
  int16_t y_coordinate_init = 0;
  int16_t wide = 10;
  int16_t high = 10;
  uint16_t colour = 0xFFFF;  // white
  int16_t x_coordinate = 0;
  int16_t y_coordinate = 0;
  int16_t i_counter = 0;
  uint16_t initialTime = 0;
  uint16_t waitTime = 0;  // Equals movement speed
};

extern struct player player_1;

/*************************************
 PLAYER PROCEDURES
 ************************************/
void player_set(uint8_t player_select, int16_t x_coordinate_init,
                int16_t y_coordinate_init, int16_t wide, int16_t high,
                uint16_t colour);

void player_init(int16_t x_coordinate_init, int16_t y_coordinate_init,
                 int16_t wide, int16_t high, uint16_t colour);

void player_move();

#endif
