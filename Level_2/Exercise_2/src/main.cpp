/**
 * LEVEL 2 - EXERCISE 2
 *
 * @author Shiris S.G.
 * @version 0.2.0 (game reference)
 * @date 2021/06/03
 * @brief Creating module: CPU_NPC (Non Playable Character)
 *
 *
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the tearms of the GNU General Public License as published by the Free
 * Software Fundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 */

#include <Arduino.h>
#include <Adafruit_GFX.h>  // Core graphics library by Adafruit
#include <Arduino_ST7789.h>  // Hardware-specific library for ST7789 (with or without CS pin)
#include <SPI.h>
#include <Adafruit_I2CDevice.h>
#include <Player.h>
#include <Screen_configuration.h>
#include <CPU_NPC.h>

int16_t i = 0;

void setup() {
  init_screen(BLACK);
  player_set(1, 0, 0, 10, 10, WHITE);

  player_init(player_1.x_coordinate_init, player_1.y_coordinate_init,
              player_1.wide, player_1.high, player_1.colour);

  NPC_set(1, 0, 120, 10, 10, GREEN);

  NPC_init(NPC_1.x_coordinate_init, NPC_1.y_coordinate_init, NPC_1.wide,
           NPC_1.high, NPC_1.colour);
}

void loop() {
  player_move();

  NPC_move(1, i, 0);
  i++;
  if (i >= 250) {
    i = 0;
  }
  delay(20);
}
