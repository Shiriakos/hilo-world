#include <CPU_NPC.h>
#include <Screen_configuration.h>

struct NPC NPC_1;

void NPC_set(uint8_t NPC_select, int16_t x_coordinate_init,
             int16_t y_coordinate_init, int16_t wide, int16_t high,
             uint16_t colour, uint16_t speed) {
  if (NPC_select == 1) {
    NPC_1.x_coordinate_init = x_coordinate_init;
    NPC_1.y_coordinate_init = y_coordinate_init;
    NPC_1.x_coordinate = 0;
    NPC_1.y_coordinate = 0;
    NPC_1.wide = wide;
    NPC_1.high = high;
    NPC_1.colour = colour;
    NPC_1.speed = speed;
  }
  // If your game has more players you will need more "if(NPC_select == ...)"
}

void NPC_init(int16_t x_coordinate_init, int16_t y_coordinate_init,
              int16_t wide, int16_t high, uint16_t colour) {
  tft.fillRect(x_coordinate_init, y_coordinate_init, wide, high, colour);
}

void NPC_move(uint8_t NPC_select, int16_t x_coordinate, int16_t y_coordinate,
              char direction) {
  if (NPC_select == 1) {
    if (direction == 'l') {
      NPC_1.x_coordinate = NPC_1.x_coordinate_init - x_coordinate;
      NPC_1.y_coordinate = NPC_1.y_coordinate_init + y_coordinate;
      if (x_coordinate <= 240) {
        tft.fillRect(NPC_1.x_coordinate, NPC_1.y_coordinate, NPC_1.wide,
                     NPC_1.high, NPC_1.colour);
      }
      tft.fillRect(NPC_1.x_coordinate + 10, NPC_1.y_coordinate, NPC_1.wide,
                   NPC_1.high, BLACK);
    }
    if (direction == 'r') {
      NPC_1.x_coordinate = NPC_1.x_coordinate_init + x_coordinate;
      NPC_1.y_coordinate = NPC_1.y_coordinate_init + y_coordinate;
      tft.fillRect(NPC_1.x_coordinate, NPC_1.y_coordinate, NPC_1.wide,
                   NPC_1.high, NPC_1.colour);
      tft.fillRect(NPC_1.x_coordinate - 10, NPC_1.y_coordinate, NPC_1.wide,
                   NPC_1.high, BLACK);
    }
    if (direction == 'u') {
      NPC_1.x_coordinate = NPC_1.x_coordinate_init + x_coordinate;
      NPC_1.y_coordinate = NPC_1.y_coordinate_init - y_coordinate;
      if (y_coordinate <= 240) {
        tft.fillRect(NPC_1.x_coordinate, NPC_1.y_coordinate, NPC_1.wide,
                     NPC_1.high, NPC_1.colour);
      }
      tft.fillRect(NPC_1.y_coordinate + 10, NPC_1.y_coordinate, NPC_1.wide,
                   NPC_1.high, BLACK);
    }

    if (direction == 'd') {
      NPC_1.x_coordinate = NPC_1.x_coordinate_init + x_coordinate;
      NPC_1.y_coordinate = NPC_1.y_coordinate_init + y_coordinate;
      tft.fillRect(NPC_1.x_coordinate, NPC_1.y_coordinate, NPC_1.wide,
                   NPC_1.high, NPC_1.colour);
      tft.fillRect(NPC_1.x_coordinate, NPC_1.y_coordinate - 10, NPC_1.wide,
                   NPC_1.high, BLACK);
    }
  }

  // If your game has more NPCs you will need more "if(NPC_select == ...)"
}
