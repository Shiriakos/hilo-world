#include <Arduino.h>
#include <Arduino_ST7789.h>

#ifndef _CPU_NPC_
#define _CPU_NPC_

//#pragma once

/*************************************
 NPC DEFINITION
 ************************************/

struct NPC {
  int16_t x_coordinate_init = 0;
  int16_t y_coordinate_init = 0;
  int16_t wide = 0;
  int16_t high = 0;
  uint16_t colour = RED;
  int16_t x_coordinate = 0;
  int16_t y_coordinate = 0;
  uint16_t initTime = 0;
  uint16_t speed = 0;  // Equals movement speed
};

extern struct NPC NPC_1;

/*************************************
 NPC PROCEDURES
 ************************************/

void NPC_set(uint8_t NPC_select, int16_t x_coordinate_init,
             int16_t y_coordinate_init, int16_t wide, int16_t high,
             uint16_t colour, uint16_t speed);

void NPC_init(int16_t x_coordinate_init, int16_t y_coordinate_init,
              int16_t wide, int16_t high, uint16_t colour);

void NPC_move(uint8_t NPC_select, int16_t x_coordinate, int16_t y_coordinate, char direction);

#endif
