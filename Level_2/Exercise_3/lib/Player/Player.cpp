
#include <Player.h>

struct player player_1;

void player_set(uint8_t player_select, int16_t x_coordinate_init,
                int16_t y_coordinate_init, int16_t wide, int16_t high,
                uint16_t colour) {
  if (player_select == 1) {
    player_1.x_coordinate_init = x_coordinate_init;
    player_1.y_coordinate_init = y_coordinate_init;
    player_1.x_coordinate = player_1.x_coordinate_init;
    player_1.y_coordinate = player_1.y_coordinate_init;
    player_1.wide = wide;
    player_1.high = high;
    player_1.colour = colour;
  }
  // If your game has more players you will need more "if(player_select == ...)"
}

void player_init(int16_t x_coordinate_init, int16_t y_coordinate_init,
                 int16_t wide, int16_t high, uint16_t colour) {
  tft.fillRect(x_coordinate_init, y_coordinate_init, wide, high, colour);
}

void player_move() {
  // MOVING RIGHT //

  if (digitalRead(rightButton) == HIGH) {
    if (player_1.x_coordinate != 230) {
      player_1.x_coordinate = player_1.x_coordinate + 1;
      tft.fillRect(player_1.x_coordinate, player_1.y_coordinate, 10, 10, WHITE);
      tft.fillRect(player_1.x_coordinate - 10, player_1.y_coordinate, 10, 10,
                   BLACK);
    }
  }
  // MOVING LEFT //

  if (digitalRead(leftButton) == HIGH) {
    if (player_1.x_coordinate != 0) {
      player_1.x_coordinate--;
      tft.fillRect(player_1.x_coordinate, player_1.y_coordinate, 10, 10, WHITE);
      tft.fillRect(player_1.x_coordinate + 10, player_1.y_coordinate, 10, 10,
                   BLACK);
    }
  }
  // MOVING UP //

  if (digitalRead(upButton) == HIGH) {
    if (player_1.y_coordinate != 0) {
      player_1.y_coordinate--;
      tft.fillRect(player_1.x_coordinate, player_1.y_coordinate, 10, 10, WHITE);
      tft.fillRect(player_1.x_coordinate, player_1.y_coordinate + 10, 10, 10,
                   BLACK);
    }
  }
  // MOVING DOWN //

  if (digitalRead(downButton) == HIGH) {
    if (player_1.y_coordinate != 230) {
      player_1.y_coordinate++;
      tft.fillRect(player_1.x_coordinate, player_1.y_coordinate, 10, 10, WHITE);
      tft.fillRect(player_1.x_coordinate, player_1.y_coordinate - 10, 10, 10,
                   BLACK);
    }
  }
}
