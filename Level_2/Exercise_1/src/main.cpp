/**
 * LEVEL 2 - EXERCISE 1
 * 
 * @author Shiris S.G.
 * @version 0.1.0
 * @date 2021/06/03
 * @brief Creating modules: Player and Screen_configuration
 *
 * 
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the tearms of the GNU General Public License as published by the Free
 * Software Fundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 */

#include <Arduino.h>
#include <Adafruit_GFX.h>  // Core graphics library by Adafruit
#include <Arduino_ST7789.h>  // Hardware-specific library for ST7789 (with or without CS pin)
#include <SPI.h>
#include <Adafruit_I2CDevice.h>
#include <Player.h>
#include <Screen_configuration.h>

void setup() {
    init_screen(BLACK);

  player_init(player_1.x_coordinate_init, player_1.y_coordinate_init,
              player_1.wide, player_1.high, player_1.colour);
}

void loop() { player_move(); }
