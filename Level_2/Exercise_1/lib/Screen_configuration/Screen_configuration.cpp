
#include <Screen_configuration.h>

Arduino_ST7789 tft = Arduino_ST7789(TFT_DC, TFT_RST);

void init_screen(uint16_t colour){
    tft.init(240, 240);
    tft.fillScreen(colour);
}
