#include <CPU_NPC.h>
#include <Screen_configuration.h>

struct NPC NPC_1;

void NPC_set(uint8_t NPC_select, int16_t x_coordinate_init,
             int16_t y_coordinate_init, int16_t wide, int16_t high,
             uint16_t colour) {
  if (NPC_select == 1) {
    NPC_1.x_coordinate_init = x_coordinate_init;
    NPC_1.y_coordinate_init = y_coordinate_init;
    NPC_1.x_coordinate = 0;
    NPC_1.y_coordinate = 0;
    NPC_1.wide = wide;
    NPC_1.high = high;
    NPC_1.colour = colour;
  }
  // If your game has more players you will need more "if(NPC_select == ...)"
}

void NPC_init(int16_t x_coordinate_init, int16_t y_coordinate_init,
              int16_t wide, int16_t high, uint16_t colour) {
  tft.fillRect(x_coordinate_init, y_coordinate_init, wide, high, colour);
}

void NPC_move(uint8_t NPC_select, int16_t x_coordinate, int16_t y_coordinate) {
  if (NPC_select == 1) {
    NPC_1.x_coordinate = NPC_1.x_coordinate_init + x_coordinate;
    NPC_1.y_coordinate = NPC_1.y_coordinate_init + y_coordinate;
    tft.fillRect(NPC_1.x_coordinate, NPC_1.y_coordinate, NPC_1.wide, NPC_1.high,
                 NPC_1.colour);
    tft.fillRect(NPC_1.x_coordinate + 10, NPC_1.y_coordinate, NPC_1.wide,
                 NPC_1.high, BLACK);
  }
  // If your game has more players you will need more "if(NPC_select == ...)"
}
