#ifndef _SCREEN_CONFIGURATION_
#define _SCREEN_CONFIGURATION_

#pragma once

#include <Arduino.h>
#include <Arduino_ST7789.h>

/*************************************
 SCREEN HARDWARE CONFIGURATION
 ************************************/

#define TFT_DC 8
#define TFT_RST 9
//#define TFT_CS    10 // only for displays with CS pin
#define TFT_MOSI 11 // for hardware SPI data pin (all of available pins)
#define TFT_SCLK 13 // for hardware SPI sclk pin (all of available pins)

/*************************************
 SCREEN OBJECT DECLARATION
 ************************************/

extern Arduino_ST7789 tft; // for display without CS pin

/*************************************
 SCREEN PROCEDURES
 ************************************/

void init_screen(uint16_t colour);


#endif
