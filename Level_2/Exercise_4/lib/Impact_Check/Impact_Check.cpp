#include <Impact_Check.h>

int8_t impactCheck() {
  if (NPC_1.x_coordinate <= player_1.x_coordinate + 10) {
    if (player_1.x_coordinate <= NPC_1.x_coordinate + 10) {
      if (NPC_1.y_coordinate <= player_1.y_coordinate + 10) {
        if (player_1.y_coordinate <= NPC_1.y_coordinate + 10) {
          tft.fillScreen(BLACK);
          player_set(1, 0, 0, 10, 10, WHITE);

          player_init(player_1.x_coordinate_init, player_1.y_coordinate_init,
                      player_1.wide, player_1.high, player_1.colour);

          NPC_set(1, 240, 0, 10, 10, BLUE);

          NPC_init(NPC_1.x_coordinate_init, NPC_1.y_coordinate_init, NPC_1.wide,
                   NPC_1.high, NPC_1.colour);
          return 1;
        }
      }
    }
  }
  return 0;
}
