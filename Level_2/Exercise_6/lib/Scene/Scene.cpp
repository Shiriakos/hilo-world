#include <Scene.h>

uint8_t ground_6x6_1[6][6] = {{1, 1, 1, 1, 1, 1}, {0, 0, 0, 0, 0, 0},
                              {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0},
                              {0, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1}};

void ground_6x6(uint16_t colour) {
  // ROW 0//

  if (ground_6x6_1[0][0] == 1) {
    tft.fillRect(0, 0, 39, 39, colour);
  } else if (ground_6x6_1[0][0] == 0) {
    tft.fillRect(0, 0, 39, 39, BLACK);
  }

  if (ground_6x6_1[0][1] == 1) {
    tft.fillRect(40, 0, 39, 39, colour);
  } else if (ground_6x6_1[0][1] == 0) {
    tft.fillRect(40, 0, 39, 39, BLACK);
  }

  if (ground_6x6_1[0][2] == 1) {
    tft.fillRect(80, 0, 39, 39, colour);
  } else if (ground_6x6_1[0][2] == 0) {
    tft.fillRect(80, 0, 39, 39, BLACK);
  }

  if (ground_6x6_1[0][3] == 1) {
    tft.fillRect(120, 0, 39, 39, colour);
  } else if (ground_6x6_1[0][3] == 0) {
    tft.fillRect(120, 0, 39, 39, BLACK);
  }

  if (ground_6x6_1[0][4] == 1) {
    tft.fillRect(160, 0, 39, 39, colour);
  } else if (ground_6x6_1[0][4] == 0) {
    tft.fillRect(160, 0, 39, 39, BLACK);
  }

  if (ground_6x6_1[0][5] == 1) {
    tft.fillRect(200, 0, 40, 39, colour);
  } else if (ground_6x6_1[0][5] == 0) {
    tft.fillRect(200, 0, 39, 39, BLACK);
  }

  // ROW 1//

  if (ground_6x6_1[1][0] == 1) {
    tft.fillRect(200, 40, 40, 39, colour);
  } else if (ground_6x6_1[1][0] == 0) {
    tft.fillRect(200, 40, 39, 39, BLACK);
  }

  if (ground_6x6_1[1][1] == 1) {
    tft.fillRect(0, 40, 39, 39, colour);
  } else if (ground_6x6_1[1][1] == 0) {
    tft.fillRect(0, 40, 39, 39, BLACK);
  }

  if (ground_6x6_1[1][2] == 1) {
    tft.fillRect(40, 40, 39, 39, colour);
  } else if (ground_6x6_1[1][2] == 0) {
    tft.fillRect(40, 40, 39, 39, BLACK);
  }

  if (ground_6x6_1[1][3] == 1) {
    tft.fillRect(80, 40, 39, 39, colour);
  } else if (ground_6x6_1[1][3] == 0) {
    tft.fillRect(80, 40, 39, 39, BLACK);
  }

  if (ground_6x6_1[1][4] == 1) {
    tft.fillRect(120, 40, 39, 39, colour);
  } else if (ground_6x6_1[1][4] == 0) {
    tft.fillRect(120, 40, 39, 39, BLACK);
  }

  if (ground_6x6_1[1][5] == 1) {
    tft.fillRect(160, 40, 39, 39, colour);
  } else if (ground_6x6_1[1][5] == 0) {
    tft.fillRect(160, 40, 39, 39, BLACK);
  }

  // ROW 3 //

  if (ground_6x6_1[2][0] == 1) {
    tft.fillRect(0, 80, 39, 39, colour);
  } else if (ground_6x6_1[2][0] == 0) {
    tft.fillRect(0, 80, 39, 39, BLACK);
  }

  if (ground_6x6_1[2][1] == 1) {
    tft.fillRect(0, 80, 39, 39, colour);
  } else if (ground_6x6_1[2][1] == 0) {
    tft.fillRect(0, 80, 39, 39, BLACK);
  }

  if (ground_6x6_1[2][2] == 1) {
    tft.fillRect(40, 80, 39, 39, colour);
  } else if (ground_6x6_1[2][2] == 0) {
    tft.fillRect(40, 80, 39, 39, BLACK);
  }

  if (ground_6x6_1[2][3] == 1) {
    tft.fillRect(80, 80, 39, 39, colour);
  } else if (ground_6x6_1[2][3] == 0) {
    tft.fillRect(80, 80, 39, 39, BLACK);
  }

  if (ground_6x6_1[2][4] == 1) {
    tft.fillRect(120, 80, 39, 39, colour);
  } else if (ground_6x6_1[2][4] == 0) {
    tft.fillRect(120, 80, 39, 39, BLACK);
  }

  if (ground_6x6_1[2][5] == 1) {
    tft.fillRect(160, 80, 39, 39, colour);
  } else if (ground_6x6_1[2][5] == 0) {
    tft.fillRect(160, 80, 39, 39, BLACK);
  }

  // ROW 4//

  if (ground_6x6_1[3][0] == 1) {
    tft.fillRect(0, 120, 39, 39, colour);
  } else if (ground_6x6_1[3][0] == 0) {
    tft.fillRect(0, 120, 39, 39, BLACK);
  }

  if (ground_6x6_1[3][1] == 1) {
    tft.fillRect(0, 120, 39, 39, colour);
  } else if (ground_6x6_1[3][1] == 0) {
    tft.fillRect(0, 120, 39, 39, BLACK);
  }

  if (ground_6x6_1[3][2] == 1) {
    tft.fillRect(40, 120, 39, 39, colour);
  } else if (ground_6x6_1[3][2] == 0) {
    tft.fillRect(40, 120, 39, 39, BLACK);
  }

  if (ground_6x6_1[3][3] == 1) {
    tft.fillRect(80, 120, 39, 39, colour);
  } else if (ground_6x6_1[3][3] == 0) {
    tft.fillRect(80, 120, 39, 39, BLACK);
  }

  if (ground_6x6_1[3][4] == 1) {
    tft.fillRect(120, 120, 39, 39, colour);
  } else if (ground_6x6_1[3][4] == 0) {
    tft.fillRect(120, 120, 39, 39, BLACK);
  }

  if (ground_6x6_1[3][5] == 1) {
    tft.fillRect(160, 120, 39, 39, colour);
  } else if (ground_6x6_1[3][5] == 0) {
    tft.fillRect(160, 120, 39, 39, BLACK);
  }

  // ROW 5//

  if (ground_6x6_1[4][0] == 1) {
    tft.fillRect(0, 160, 39, 39, colour);
  } else if (ground_6x6_1[4][0] == 0) {
    tft.fillRect(0, 160, 39, 39, BLACK);
  }

  if (ground_6x6_1[4][1] == 1) {
    tft.fillRect(0, 160, 39, 39, colour);
  } else if (ground_6x6_1[4][1] == 0) {
    tft.fillRect(0, 160, 39, 39, BLACK);
  }

  if (ground_6x6_1[4][2] == 1) {
    tft.fillRect(40, 160, 39, 39, colour);
  } else if (ground_6x6_1[4][2] == 0) {
    tft.fillRect(40, 160, 39, 39, BLACK);
  }

  if (ground_6x6_1[4][3] == 1) {
    tft.fillRect(80, 160, 39, 39, colour);
  } else if (ground_6x6_1[4][3] == 0) {
    tft.fillRect(80, 160, 39, 39, BLACK);
  }

  if (ground_6x6_1[4][4] == 1) {
    tft.fillRect(120, 160, 39, 39, colour);
  } else if (ground_6x6_1[4][4] == 0) {
    tft.fillRect(120, 160, 39, 39, BLACK);
  }

  if (ground_6x6_1[4][5] == 1) {
    tft.fillRect(160, 200, 39, 39, colour);
  } else if (ground_6x6_1[4][5] == 0) {
    tft.fillRect(160, 200, 39, 39, BLACK);
  }

  // ROW 6 //

  if (ground_6x6_1[5][0] == 1) {
    tft.fillRect(0, 200, 39, 39, colour);
  } else if (ground_6x6_1[5][0] == 0) {
    tft.fillRect(0, 200, 39, 39, BLACK);
  }

  if (ground_6x6_1[5][1] == 1) {
    tft.fillRect(40, 200, 39, 39, colour);
  } else if (ground_6x6_1[5][1] == 0) {
    tft.fillRect(40, 200, 39, 39, BLACK);
  }

  if (ground_6x6_1[5][2] == 1) {
    tft.fillRect(80, 200, 39, 39, colour);
  } else if (ground_6x6_1[5][2] == 0) {
    tft.fillRect(80, 200, 39, 39, BLACK);
  }

  if (ground_6x6_1[5][3] == 1) {
    tft.fillRect(120, 200, 39, 39, colour);
  } else if (ground_6x6_1[5][3] == 0) {
    tft.fillRect(120, 200, 39, 39, BLACK);
  }

  if (ground_6x6_1[5][4] == 1) {
    tft.fillRect(160, 200, 39, 39, colour);
  } else if (ground_6x6_1[5][4] == 0) {
    tft.fillRect(160, 200, 39, 39, BLACK);
  }
  if (ground_6x6_1[5][5] == 1) {
    tft.fillRect(200, 200, 39, 39, colour);
  } else if (ground_6x6_1[5][5] == 0) {
    tft.fillRect(200, 200, 39, 39, BLACK);
  }
}
