/**
 * LEVEL 2 - EXERCISE 6
 *
 * @author Shiris S.G.
 * @version 0.6.0 (game reference)
 * @date 2021/06/08
 * @brief Scenario.h
 *
 *
 *
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the tearms of the GNU General Public License as published by the Free
 * Software Fundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 */

#include <Arduino.h>
#include <Adafruit_GFX.h>  // Core graphics library by Adafruit
#include <Arduino_ST7789.h>  // Hardware-specific library for ST7789 (with or without CS pin)
#include <SPI.h>
#include <Adafruit_I2CDevice.h>
#include <Player.h>
#include <Screen_configuration.h>
#include <CPU_NPC.h>
#include <Impact_Check.h>
#include <Scene.h>

/*************************************
 USER MACROS
 ************************************/

#define Player_1_speed 20
#define NPC_1_speed 20

/*************************************
 USER VARIABLES
 ************************************/

int16_t i = 0;
uint16_t currentTime = 0;

void setup() {
  init_screen(BLACK);

  player_set(1, 0, 41, 10, 10, WHITE, Player_1_speed);

  player_init(player_1.x_coordinate_init, player_1.y_coordinate_init,
              player_1.wide, player_1.high, player_1.colour);

  NPC_set(1, 240, 100, 10, 10, GREEN, NPC_1_speed);

  NPC_init(NPC_1.x_coordinate_init, NPC_1.y_coordinate_init, NPC_1.wide,
           NPC_1.high, NPC_1.colour);

  ground_6x6(GREEN);
}

void loop() {
  currentTime = millis();

  if (currentTime - player_1.initTime >= player_1.speed) {
    player_move();
    player_1.initTime = millis();
  }

  if (currentTime - NPC_1.initTime >= NPC_1.speed) {
    NPC_move(1, -i, 0);  // -i if you want left move.
    NPC_1.initTime = millis();
    i++;
    if (i >= 240) {
      i = 0;
    }
  }

  if (impactCheck() == 1) {
    i = 0;
  }
}

/* Exercise:
- Draw new scenarios
*/
