# Exercise index

## Level 1: Basics
---  

### Exercise 1: Drawing rectangles and lines.
### Exercise 2 - For loop: introduction.
### Exercise 3 - For loop: Drawing horizontal lines.
### Exercise 4 - For loop: Changing size of filled rectangle.
### Exercise 5 - For loop: Drawing a white frame.
### Exercise 6 - For loop: Drawing a white filled rectangle moving along the edge of the screen
### Exercise 7 - If structure and digitalRead () function: Moving a white filled rectangle using buttons.
### Exercise 8 - If structure: Setting movement restrictions.
### Exercise 9 - Own procedures: Creating a function for each button and direction of movement.
### Exercise 10 - Own procedures: Creating a function for each moving obstacle.
### Exercise 11 - Own procedures with parameters: Creating a generic function for moving obstacle.
### Exercise 12 - Timer with millis() function: Alternative to delay().
### Exercise 13 - Timer with millis() function: player and obstacles with timers.
### Exercise 14 - Own Struct: Creating the "struct obstacle".
### Exercise 15 - Improving the code.
### Exercise 16 - Own Struct: Creating the "struct player".
### Exercise 17 - Improving the code.
### Exercise 18 - Improving the code.
### Exercise 19 - Own procedures with parameters: Creating a generic function with direction parameter for moving obstacle.  

---
## Level 2: Modular programming - First game
---  

### Exercise 1 - Screen_configuration and Player modules.
### Exercise 2 - CPU_NPC module.
### Exercise 3 - Impact_Check module X-Axis.
### Exercise 4 - Impact_Check module Y-Axis.
### Exercise 5 - Added timer and speed attribute in Player and NPC structs.
### Exercise 6 - Added a simple scene module.
### Exercise 7 - Fixed CPU_NPC left and up moving. Improved NPC_move function.
### Exercise 8 - Set up and down limits for player 1, created 4 NPCs and disabled impact check (it will be activaded in next version).